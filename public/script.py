
import json
import requests
from flask import Flask, jsonify, request
from pymongo import MongoClient
from flask_restful import Api
from flask_cors import CORS

def getYtbData(key):
    ytbData = requests.get("https://www.googleapis.com/youtube/v3/videos?part=snippet&id="+str(key)+"&key=AIzaSyCaq481561qeqvBDJhaEsQlubHSD1b0aKY")
    return ytbData.json()

def getResponseCheck(req):
    linkSource = requests.post("https://api.download-yt-mp3.top/"+req['videoId']+"?type="+req['videoType']).json()
    if str(linkSource['type']) != "onDatabase" : 
        return jsonify({'success': linkSource})   
    ytData = getYtbData(req['videoId'])
    link = {
        'videoId': req['videoId'],
        'title': ytData['items'][0]['snippet']['title'],
        'description': ytData['items'][0]['snippet']['description'],
        'thumbnails': ytData['items'][0]['snippet']['thumbnails'],
        'size': linkSource['size']
    }  
    link['linkSource'] = linkSource['secureLink']
    return jsonify(link)


app = Flask(__name__)
CORS(app)
app.debug = True
@app.route('/')
def home():
    return "getResponseCheck(req)", 200
    
@app.route('/api/check',  methods=['post'])
def check():
    req = request.json
    return getResponseCheck(req), 200

@app.route('/api/convert',  methods=['post'])
def convert():
    req = request.json
    lik = str(req['domain'])+'/convert/'+str(req['hash']).replace("\n", "\\n")
    isConvert = requests.get(lik).json()
    return jsonify(isConvert), 200

@app.route('/api/progress',  methods=['post'])
def progress():
    req = request.json
    lik = str(req['domain'])+'/progress/'+str(req['hash']).replace("\n", "\\n")
    isProgress= requests.get(lik).json()
    return jsonify(isProgress), 200

@app.route('/api/search',  methods=['post'])
def search():
    req = request.json
    URL="http://suggestqueries.google.com/complete/search?client=firefox&ds=yt&q="+req['key']
    headers = {'User-agent':'Mozilla/5.0'}
    response = requests.get(URL, headers=headers)
    result = json.loads(response.content.decode('utf-8'))
    print(result)
    return jsonify(result), 200
    
@app.route('/api/test',  methods=['post'])
def testpost():
    return "test"
if __name__ == "__main__":
    app.run()
