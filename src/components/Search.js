import React, {Component} from 'react';
import {AutoComplete} from 'primereact/autocomplete';
import {Button} from 'primereact/button';
import axios from 'axios'
import { Link } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css'

const isYoutubeUrl = (url) => {
    return /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|playlist\?list=|&list=|watch\?v=|&v=)([^#&?]*).*/.test(url)
}
const generrHash = () => {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/*-+&~_%!?";
    for (var i = 0; i < 150; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text.toString(32)
}

export default class Search extends Component {
constructor() {
    super();
    this.state = {
        suggestions: null,
        location: {
            pathname: '/search',
            search: '',
            state: {}
        }
    };
}
onChange = (event) => {
    this.setState({
        key: event.value
    });
    this.state.location.state = {key: event.value}
    if (!isYoutubeUrl(event.value)) this.state.location.search = "?hash="+generrHash()
}
suggests = (event) => {
    if(!isYoutubeUrl(event.query))  
        axios.post('http://yoump3tubeapp.ga/api/search',{key: event.query+""})
        .then((response) => {
                let results = response.data[1]
                results.splice(0, 0, response.data[0]);
                this.setState({ suggestions: results });
            })
}

render() {    
    return (
        <div>
            <form>
                <AutoComplete value={this.state.key} onChange={this.onChange}
                    inputStyle={{width: "100%",height:"50px",borderRadius:"3px 0 0 3px"}} style={{width: "70%"}} suggestions={this.state.suggestions} completeMethod={this.suggests.bind(this)} />
                <Link to={this.state.location}>
                    <Button label="" icon="pi pi-search" className="ui-button-danger" iconPos="left" style={{padding: "11px 25px",borderRadius:"0 3px 3px 0",border: "1px solid #d6d6d6",borderLeft: "0px solid #d6d6d6"}} />
                </Link>
            </form>
            <style jsx="true">{`
                .ui-autocomplete-panel .ui-autocomplete-list-item:hover {
                    border-color: #eee; 
                    background: #eee;
                    color: #222;
                }
                input:focus{
                    -webkit-box-shadow: 0px 0px 0px #d9534f !important;
                    box-shadow: 0px 0px 0px #d9534f !important;
                }
            `}</style>
        </div>
    );
}   

}