import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import '../style/header.css'
import 'font-awesome/css/font-awesome.css'

export default class Header extends Component {
    render() {
        return(
            <div>
                <div className="ui-g bg-header">
                    <div className="ui-g-12 ui-md-1 ui-lg-2"></div>
                    <div className="ui-g-12 ui-md-10 ui-lg-8 items">
                        <Link to="/" className="item transition active">
                                <i className="fa fa-home"></i>
                                <span className="item-label transition">
                                    Home
                                </span>
                        </Link>
                        <Link to="/top_hits" className="item transition ">
                                <i className="fa fa-heartbeat"></i>
                                <span className="item-label transition">
                                    Top Hits
                                </span>
                        </Link>
                        <Link to="/guide" className="item transition ">
                                <i className="fa fa-life-ring"></i>
                                <span className="item-label transition">
                                    Guide
                                </span>
                        </Link>
                        <Link to="/terms" className="item transition ">
                                <i className="fa fa-gavel"></i>
                                <span className="item-label transition">
                                    Terms
                                </span>
                        </Link>
                        <Link to="/copyright" className="item transition ">
                                <i className="fa fa-copyright"></i>
                                <span className="item-label transition">
                                    Copyright
                                </span>
                        </Link>
                    </div>
                    <div className="ui-g-12 ui-md-1 ui-lg-2"></div>
                </div>
            </div>
        )
    }
}