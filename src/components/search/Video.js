import React, { Component } from 'react';
import { Link } from 'react-router-dom'
const generrHash = () => {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/*-+&~_%!?";
    for (var i = 0; i < 150; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text.toString(32)
}
export default class Video extends Component {
    constructor(props){
        super(props)
        this.state = {
            locationMp3: {
                pathname: '/download',
                search: "?hash=" + generrHash(),
                state: {
                    type: 'mp3',
                    videoId: props.videoId
                }
            },
            locationMp4: {
                pathname: '/download',
                search: "?hash=" + generrHash(),
                state: {
                    type: 'mp4',
                    videoId: props.videoId
                }
            }
        };
    }
    render() {
        return(
            <article className="row rowArticle">
                <div className="col-3 gauche">
                    <a title={this.props.title}>
                        <img className="videoThumb" src={'https://i.ytimg.com/vi/'+this.props.videoId+'/hqdefault.jpg'} alt={this.props.videoId} />
                    </a>
                </div>
                <div className="col-8 pl-2">
                    <h2>{this.props.title}</h2>
                    <p>{this.props.description}</p>
                </div>
                <div className="col-1">
                    <Link to={this.state.locationMp3}><div className="col-6"><button className="btn-red"><i className="fa fa-cloud-download"></i>MP3</button></div></Link>
                    <Link to={this.state.locationMp4}><div className="col-6"><button className="btn-blue"><i className="fa fa-cloud-download"></i>MP4</button></div></Link>
                </div>
                
			</article>
        )
    }
}