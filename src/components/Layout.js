import React, { Component } from 'react';
import Search from './Search';
import Header from './Header';
import Footer from './Footer';

import '../style/layout.css';
export default class Layout extends Component {
    render() {
        return(
            <div>
                <Header />
                <div className="container margin-cont">
                    <div className="col-lg-10 col-md-12 col-sm-12 offset-lg-1 mt-5 text-center">
                        <img className="mt-5" src="/static/logo.png" width="100" alt="YouMP3Tube" />
                        <h3 className="sousLogo"><strong style={{color: "#d9534f"}}>YouMP3Tube</strong>, N°1 <i className="fa fa-youtube-play" style={{color: "#d9534f"}}></i> Youtube conversion to MP3, MP4</h3>
                        <Search />
                        <h3 className="sousLogo mt-2">Convert and Download Youtube videos to MP3, MP4</h3>
                        <div className="mt-1 mb-3">
                            <div id="fb-root"></div>
                            <iframe src="https://www.facebook.com/plugins/like.php?href=http://localhost:3000/&width=202&layout=button&action=recommend&size=large&show_faces=false&share=true&height=65&appId" width="202" height="65" scrolling="no" frameBorder="0" allowtransparency="true" allow="encrypted-media" title="FB"></iframe>
                        </div>
                        {this.props.children}
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}