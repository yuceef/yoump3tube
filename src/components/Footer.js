import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import '../style/footer.css'
import 'bootstrap/dist/css/bootstrap.min.css'

export default class Footer extends Component {
    render() {
        return(
            <div className="bg-footer mt-5">
                <div className="col-md-8 col-lg-6 text-center offset-md-2 offset-lg-3 row">
                    <div className="col-3 text-center">
                        <h5>YouMP3Tube</h5>
                        <Link to="/"  className="link">Home</Link>
                        <Link to="/top_hits" className="link">Top Hits</Link>
                        <Link to="/guide" className="link">Guide</Link>
                        <Link to="/terms" className="link">Terms</Link>
                        <Link to="/copyright" className="link">Copyright</Link>
                    </div>
                    <div className="col-3 text-center">
                        <h5>Apps</h5>
                        <a className="link"><i className="fa fa-apple" aria-hidden="true"></i> iOS</a>
                        <a className="link"><i className="fa fa-android" aria-hidden="true"></i> Android</a>
                        <a className="link"><i className="fa fa-windows" aria-hidden="true"></i> Windows Mobile</a>
                        <a className="link"><i className="fa fa-refresh" aria-hidden="true"></i> Browser Extensions</a>
                    </div>
                    <div className="col-3 text-center">
                        <h5>Links</h5>
                    </div>
                    <div className="col-3 text-center">
                        <h5>Languages</h5>
                        <a href="/" className="link">English</a>
                        <a href="https://translate.google.com/translate?hl=&sl=en&tl=fr&u=https://petrolicious.com&sandbox=1" className="link">Français</a>
                        <a href="https://translate.google.com/translate?hl=&sl=en&tl=es&u=https://petrolicious.com&sandbox=1" className="link">Español</a>
                        <a href="https://translate.google.com/translate?hl=&sl=en&tl=pt&u=https://petrolicious.com&sandbox=1" className="link">Português</a>
                        <a href="https://translate.google.com/translate?hl=&sl=en&tl=zh-CN&u=https://petrolicious.com&sandbox=1" className="link">简体中文</a>
                    </div>
                </div>
            </div>
        )
    }
}