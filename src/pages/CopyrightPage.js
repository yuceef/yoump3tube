import React, { Component } from 'react';
import Layout from '../components/Layout'
import {Card} from 'primereact/card';

export default class CopyrightPage extends Component {
    render() {
        return(
            <Layout>
                <Card  className="mt-2 col-md-12">
                    <h3 align="left"><i className="fa fa-copyright"></i> Copyright</h3>
                    <p align="left">
                    We are aware that alot of Youtube content is under copyright, are a copyright owner or an agent thereof, and you believe that any content downloaded on our web site infringes your copyrights, please send us a notification pursuant to the Digital Millennium Copyright Act ("DMCA") by providing:
                    <br />
                    - A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed
                    <br />
                    - The URL(s) of the video(s) you want us to block
                    <br />
                    To this email: copyright@gmail.com
                    <br />
                    We'll disable it's download on Tym within 48h in business days.
                    </p>
                </Card>
                <style jsx="true">{`
                    .ui-card-body i{
                        font-size: 1.75rem !important
                    }
                    
                `}</style>
            </Layout>
        )
    }
} 
