import React, { Component } from 'react';
import Layout from '../components/Layout'
import {Card} from 'primereact/card';

export default class TermsPage extends Component {
    render() {
        return(
            <Layout>
                <Card  className="mt-2 col-md-12">
                    <h3 align="left"><i className="fa fa-gavel"></i> Terms & conditions</h3>
                    <p align="left">
                    By accessing or using this website, you agree to be bound by its terms of use as set forth in this document.
                    <br />
                    We reserve the right to change these terms at any time and your acceptance.
                    <br />
                    You agree not to download any illegal or copyrighted video that may compromise the integrity or availability of our system.
                    <br />
                    You agree not to disrupt the servers hosting this service and not to access them by an automated system such as robots that send more requests over a period of time than a user can reasonably produce during the same period.
                    The navigation on this site also allows our advertising partners to add and use cookies on your browser, these cookies are added to regulate the display of ads on your device.                    </p>
                </Card> 
                <style jsx="true">{`
                    .ui-card-body i{
                        font-size: 1.75rem !important
                    }
                    
                `}</style>
            </Layout>
        )
    }
} 