import React, { Component } from 'react';
import Layout from '../components/Layout'
import {Card} from 'primereact/card';

export default class GuidePage extends Component {
    render() {
        return(
            <Layout>
                <Card  className="mt-2 col-md-12">
                    <h3 align="left"><i className="fa fa-life-ring"></i> Guide</h3>
                    <ol className="text-left pl-3">
                        <li>Paste the link to a Youtube video or write the title of a music.</li>
                        <li>Click the button to initiate a search of the media.</li>
                        <li>Choose MP3 or MP4 type.</li>
                        <li>It's ready, you just need click on the download button.</li>
                    </ol>
                </Card>
                <style jsx="true">{`
                    .ui-card-body i{
                        font-size: 1.75rem !important
                    }
                    
                `}</style>
            </Layout>
        )
    }
} 
