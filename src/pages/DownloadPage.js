import React, { Component } from 'react';
import Layout from '../components/Layout'
import axios from 'axios';
import { Redirect } from 'react-router-dom'
import { ProgressBar } from 'primereact/progressbar';
import { Card } from 'primereact/card';

export default class DownlodPage extends Component {
    constructor(props){
        super();
        this.state = {
            progress: '0%',
            myData: {},
            type: props.location.state.type,
            videoId: props.location.state.videoId
        }  
    }
    componentWillMount = () => {
        this.Check()
    }
    Check = async () => {
        const data = {
            videoId: this.state.videoId,
            videoType: this.state.type,
        }
        axios.post('http://yoump3tubeapp.ga/api/check', data).then(res => {
            var dataOfVideo = res.data.success
            if (dataOfVideo) {
                if (dataOfVideo.type === 'toBeConverted') {
                    this.Convert(dataOfVideo)
                }
                else if (dataOfVideo.type === 'brokeLimit') {
                    alert("You have reached limit of 40 downloads in 10 minutes!");
                    Redirect(303, '/')
                }
                else if (dataOfVideo.type === 'tooLong') {
                    alert(`Sorry, the video you're trying to convert is too long`)
                    Redirect(303, '/')
                }
            }
            else {
                this.setState({ myData: res.data, link: res.data.linkSource, progress: null })
            }
        })
    }
    Convert = (dataOfVideo) => {
        axios.post('http://yoump3tubeapp.ga/api/convert', dataOfVideo).then(res => {
            if (res.data.success) {
                this.Progress(dataOfVideo)
            }
        })
    }
    Progress = (dataOfVideo) => {
        axios.post('http://yoump3tubeapp.ga/api/progress', dataOfVideo).then(res => {
            if (res.data.success) {
                if (res.data.progress) {
                    this.setState({ progress: res.data.progress + "%" })
                }
                if (res.data.converting) {
                    this.setState({ progress: "Conversion in progress..." })
                }
                setTimeout(
                    this.Progress(dataOfVideo)
                    , 500);
            }
            if (res.data.hash) {
                this.Check()
            }
        })
    }
    render() {
        return(
            <Layout>
                {
                    (this.state.videoId && this.state.type && this.state.progress) ?
                        <div className= "container">
                            <ProgressBar value={this.state.progress} style={{ height: '30px',backgroud: 'red' }} mode="indeterminate"></ProgressBar>
                        </div>
                        :
                        <h3 className="sousLogo mt-3 mb-2">{this.state.myData.title}</h3>
                }
                {
                    (this.state.myData.linkSource) ? 
                        <div className="text-center col-sm-4 offset-sm-4">
                            <a href={this.state.myData.linkSource}>
                                <button className="btn btn-danger btn-block">
                                    Download {this.state.type} <br /> Size {this.state.myData.size} Mb 
                                </button>
                            </a>
                        </div> 
                        : 
                        null
                }
                <div className="fb-save mt-3 " data-uri="https://www.instagram.com/facebook/" data-size="large"></div>
                <div className="videowatch mt-5">
                    <iframe className="form-group" width="520" height="320" src={'https://www.youtube.com/embed/' + this.state.videoId} frameBorder="0" title={this.state.videoId}></iframe>
                    {(this.state.myData.description) ? <Card className="text-justify container mb-5">{this.state.myData.description}</Card>:null}
                </div>
                <style jsx="true">
                {`
                    .ui-progressbar .ui-progressbar-value {
                        border: 0 none;
                        margin: 0;
                        background: #d9534f !important;
                    }
                `}
                </style>

            </Layout>
        )
    }
}