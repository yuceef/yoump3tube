import React, { Component } from 'react';
import {Panel} from 'primereact/panel';
import Layout from '../components/Layout'

export default class Home extends Component {
    render() {
        return(
            <Layout>
                <div className="col-sm-12 mb-5 row">
                    <div className="mb-5 col-md-8">
                        <Panel header="Convert and download Youtube" toggleable={true} className="mt-2 col-md-12">
                            <p className="text-left">
                                Have you ever needed a fast and reliable YouTube to mp3 converter to download videos and favourite tracks? Ever tried to find an easy alternative way to get your video files for free? Ever got tired of “easy” malware-infected sites, that ask you for registration and demand fees at the last step of the process? Having trouble finding a good alternative converter for your phone? Look no further, <strong style={{color: "#d9534f"}}>YouMP3Tube</strong> is exactly what you need.
                            </p>
                            <p className="text-left">
                                We provide an online service that converts your videos to mp3 and other formats from YouTube in just a couple of clicks. All you need in order to get mp3 is to paste a URL into the paste area on our website and then our converter will do the rest! Your conversion starts immediately!
                            </p>
                        </Panel>
                        <Panel header="Advantages of this Youtube Converter" toggleable={true} className="mt-2 col-md-12">
                            <p className="text-left">
                                All downloads and searches that you do on our platform are anonymous. All data are encrypted, no file is stocked and no history is saved in our database. You can convert mp3 with no limits and for free, you will not be bothered by ads and commercials. We are the fastest service, the audio file download is almost done in one click.
                            </p>
                        </Panel>
                        <Panel header="Simple, Fast and High-Efficiency!" toggleable={true} className="mt-2 col-md-12">
                            <p className="text-left">Start converting videos from YouTube to MP3 and mp4 formats with the number 1 online converter! <strong style={{color: "#d9534f"}}>YouMP3Tube</strong> is totally free, fast and easy to use.</p>
                            <p className="text-left">All you have to do is simply copy and paste your link from Youtube to this website’s bar and get everything done in few seconds!</p>
                            <p className="text-left">Don’t waste your time looking for another tool because we guarantee the best output quality you will ever want!</p>
                        </Panel>
                    </div>
                    <div className="col-md-4">
                        <Panel header="Instructions" toggleable={true} className="mt-2 col-md-12">
                            <ol className="text-left pl-3">
                                <li>Paste the link to a Youtube video or write the title of a music.</li>
                                <li>Click the button to initiate a search of the media.</li>
                                <li>Choose MP3 or MP4 type.</li>
                                <li>It's ready, you just need click on the download button.</li>
                            </ol>
                        </Panel>
                        <Panel header="Our services" toggleable={true} className="mt-2 col-md-12">
                            <ul className="text-left pl-0">
                                <li>Free service</li>
                                <li>No software is required</li>
                                <li>No need to register</li>
                                <li>Compatibility with mobile</li>
                                <li>No download limits</li>
                                <li>Instant audio conversion</li>
                                <li>Anonymous downloads</li>
                            </ul>
                        </Panel>
                    </div>
                </div>
                <style jsx="true">{`
                    p{
                        font-family: 'montserrat', 'open_sans';
                        font-size:17px;
                        line-height: 20px;
                    }
                    ul.text-left  {
                        list-style: none;
                    }
                    .text-left li {
                        font-size: 16px;
                        line-height: 16px;
                        padding: 4px 0;
                        color: #3E3E3E;
                        font-family: 'montserrat', 'open_sans';
                      }
                    ul.text-left  li:before {
                        content: '✓';
                        padding-right:5px;
                     }
                `}</style>
            </Layout>
        )
    }
}