import React, { Component } from 'react';
import Layout from '../components/Layout'

export default class SoonPage extends Component {
    render() {
        return(
            <Layout>
                <img src="/static/Coming-Soon.png" alt="Soon" className="container mt-5 mb-5" />
            </Layout>
        )
    }
} 
