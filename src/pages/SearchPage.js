import React, { Component } from 'react';
import Layout from '../components/Layout'
import Video from '../components/search/Video'
import axios from 'axios';

import '../style/search.css'
const isYoutubeUrl = (url) => {
    return /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|playlist\?list=|&list=|watch\?v=|&v=)([^#&?]*).*/.test(url)
}

export default class SearchPage extends Component {
    constructor(props){
        super();
        this.state = {
            key: props.location.state.key
        }        
    }
    componentWillMount = () => {
        var key = this.state.key
        this.setState({
            video: "",
            videos: "",
            list: ""
        })
        if (isYoutubeUrl(key)) {
            var url = key
            var reg = new RegExp("[&?]list=([a-z0-9_-]+)", "i");
            var match = reg.exec(url);
            if (match && match[1].length > 0) {
                axios.get('https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&type=video&maxResults=50&key=AIzaSyCaq481561qeqvBDJhaEsQlubHSD1b0aKY&playlistId=' + match[1])
                .then(response =>{
                    this.setState({
                        list:  response.data.items
                    })
                })
            }
            reg = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            match = url.match(reg);
            if (match && match[2].length === 11) {
                axios.get('https://www.googleapis.com/youtube/v3/videos?part=snippet&type=video&key=AIzaSyCaq481561qeqvBDJhaEsQlubHSD1b0aKY&id=' + match[2])
                .then((response) => {   
                    this.setState({
                        video:  response.data.items[0]
                    })
                })
            }
        }
        else { 
            axios.get('https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&key=AIzaSyCaq481561qeqvBDJhaEsQlubHSD1b0aKY&order=viewCount&maxResults=10&q=' + key)
            .then(response =>{
                this.setState({
                    videos:  response.data.items
                }) 
            })
        }
    }
    componentWillUpdate = (props) => {
        if(this.state.key !== props.location.state.key){
            this.setState({
                key: props.location.state.key,
                video: "",
                videos: "",
                list: ""
            })
            if (isYoutubeUrl(props.location.state.key)) {
                var url = props.location.state.key
                var reg = new RegExp("[&?]list=([a-z0-9_-]+)", "i");
                var match = reg.exec(url);
                if (match && match[1].length > 0) {
                    console.log();
                    
                    axios.get('https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&type=video&maxResults=50&key=AIzaSyCaq481561qeqvBDJhaEsQlubHSD1b0aKY&playlistId=' + match[1])
                    .then(response =>{
                        this.setState({
                            list:  response.data.items
                        })
                    })            
                }
                reg = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                match = url.match(reg);
                if (match && match[2].length === 11) {
                    axios.get('https://www.googleapis.com/youtube/v3/videos?part=snippet&type=video&key=AIzaSyCaq481561qeqvBDJhaEsQlubHSD1b0aKY&id=' + match[2])
                    .then((response) => {            
                        this.setState({
                            video:  response.data.items[0]
                        })
                    })
                }
            }
            else { 
                axios.get('https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&key=AIzaSyCaq481561qeqvBDJhaEsQlubHSD1b0aKY&order=viewCount&maxResults=10&q=' + props.location.state.key)
                .then(response =>{
                    this.setState({
                        videos:  response.data.items
                    })
                })
            }  
        } 
    }
    render() {  
        return(
            <Layout>
                <div className="videosSearch">
                    {
                        (this.state.video)?
                            <Video videoId={this.state.video.id} title={this.state.video.snippet.title} description={this.state.video.snippet.description} />

                        :

                            null
                    }
                    {
                        (this.state.videos)?

                            this.state.videos.map((video,i) => 
                                <Video key={i} videoId={video.id.videoId} title={video.snippet.title} description={video.snippet.description}  />
                            )
                    
                        :

                            null
                    }
                    {
                        (this.state.list)?

                            this.state.list.map((video,i) => 
                                <Video key={i} videoId={video.snippet.resourceId.videoId} title={video.snippet.title} description={video.snippet.description}  />
                            )
                    
                        :

                            null
                    }
                </div>
            </Layout>
            
        )
}
}