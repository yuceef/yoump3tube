import React, { Component } from 'react';
import { Route, BrowserRouter, Switch} from 'react-router-dom'

// Components
import HomePage from './pages/HomePage'
import SearchPage from './pages/SearchPage'
import DownloadPage from './pages/DownloadPage'
import SoonPage from './pages/SoonPage'
import GuidePage from './pages/GuidePage'
import TermsPage from './pages/TermsPage'
import CopyrightPage from './pages/CopyrightPage'
class App extends Component {
    render() {
        return (
          <BrowserRouter>
            <Switch>
              <Route path="/search"  component={SearchPage}></Route>
              <Route path="/download"  component={DownloadPage}></Route>
              <Route path="/guide"  component={GuidePage}></Route>
              <Route path="/terms"  component={TermsPage}></Route>
              <Route path="/copyright"  component={CopyrightPage}></Route>
              <Route path="/" exact component={HomePage}></Route>
              <Route path='*' component={SoonPage} />
            </Switch>
          </BrowserRouter>
        );
    }
}

export default App;
