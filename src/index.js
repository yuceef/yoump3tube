// React
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'
import ErrorBoundary from './pages/ErrorBoundary'

// Style
import './style/index.css';
import 'primereact/resources/themes/omega/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primereact/components/grid/Grid.css';

import registerServiceWorker from './registerServiceWorker';

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v3.1';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

ReactDOM.render(<ErrorBoundary><App /></ErrorBoundary> , document.getElementById('root'));
registerServiceWorker();
